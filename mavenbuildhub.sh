docker run -it --rm --name my-maven-project -v "$(pwd)":/usr/src/mymaven -w /usr/src/mymaven maven:3.3-jdk-8 mvn clean install
docker run --name some-mysql -e MYSQL_DATABASE=assignment -e  MYSQL_ROOT_PASSWORD=P@ssw0rd -d mysql:5.7
 docker run -it -p 80:8090 -e MYSQL_ADDR=some-mysql --link some-mysql -e MYSQL_DATABASE=assignment -e  MYSQL_ROOT_PASSWORD=P@ssw0rd javatest2