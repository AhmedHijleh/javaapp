FROM maven:3.3-jdk-8
LABEL Ahmed Abu_Hijleh (ahmed.abuhijleh@progressoft.com)
EXPOSE 8080
COPY  target/assignment*.jar /app/app.jar
ENTRYPOINT ["java", "-jar", "/app/app.jar" ] 

#java -jar -Dserver.port=8070 -Dspring.datasource.username=newuser  -Dspring.datasource.password